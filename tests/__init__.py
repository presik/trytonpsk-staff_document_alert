# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from .test_staff_document_alert import suite

__all__ = ['suite']
