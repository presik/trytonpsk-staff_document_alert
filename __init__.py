# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .employee import (Employee, KindDocument, Alert, Contract)

def register():
    Pool.register(
        Employee,
        KindDocument,
        Alert,
        Contract,
        module='staff_document_alert', type_='model')
