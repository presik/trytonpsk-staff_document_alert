# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import timedelta, date
from trytond.pool import Pool, PoolMeta
from trytond.model import fields, ModelSQL, ModelView
from trytond.transaction import Transaction


__all__ = ['Employee', 'KindDocument', 'Alert', 'Contract']


class Employee:
    __metaclass__ = PoolMeta
    __name__ = 'company.employee'
    documents = fields.One2Many('staff.document', 'employee',
        'Documents')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()

    @classmethod
    def send_document_alert(cls):
        pool = Pool()
        Template = pool.get('electronic.mail.template')
        templates = Template.search([
            ('model.model', '=', 'company.employee'),
        ])
        if not templates:
            return

        employees = cls.search([
                ('active', '=', True),
                ])

        today = date.today()
        alerts_to_send = []
        for template in templates:
            for employee in employees:
                for doc in employee.documents:
                    for alert in doc.kind.alerts:
                        if alert.days == 0 or doc.date_end is None:
                            continue
                        delta = (doc.date_end - timedelta(alert.days))
                        if delta > today:
                            continue
                        elif alert.frecuency == 'once':
                            if delta == today:
                                alerts_to_send.append(doc)
                                continue
                        else:
                            alerts_to_send.append(doc)

        context = Transaction().context.copy()
        with Transaction().set_context(context):
            template.render_and_send(alerts_to_send)


class KindDocument:
    __metaclass__ = PoolMeta
    __name__ = 'staff.kind_document'
    alerts = fields.One2Many('staff.kind_document.alert',
            'kind_document', 'Alerts')


class Alert(ModelSQL, ModelView):
    'Staff Kind Document Alert'
    __name__ = 'staff.kind_document.alert'
    kind_document = fields.Many2One('staff.kind_document', 'Kind Document',
        required=True)
    days = fields.Integer('Days', required=True)
    frecuency = fields.Selection([
            ('always', 'Always'),
            ('once', 'Once'),
            ], 'Frecuency')


class Contract:
    __metaclass__ = PoolMeta
    __name__ = 'staff.contract'

    @classmethod
    def send_contracts_alert(cls):
        pool = Pool()
        Template = pool.get('electronic.mail.template')
        templates = Template.search([
            ('model.model', '=', 'staff.contract'),
        ])

        if not templates:
            return

        contracts = cls.search([
                ('state', '=', 'active'),
        ])

        alerts_to_send = []
        for template in templates:
            for contract in contracts:
                # FIXME: Fix hard coded value connstant value
                days_finished = contract.get_time_for_finished()
                if not days_finished or days_finished > 10:
                    continue
                alerts_to_send.append(contract)

        context = Transaction().context.copy()
        with Transaction().set_context(context):
            template.render_and_send([alerts_to_send])
